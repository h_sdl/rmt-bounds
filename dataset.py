# -*- coding: utf-8 -*-
"""
* This file can be used a library used to
* generate data (class vectors and data around them)
"""

import numpy as np
eps = 1e-6

def generate_class_vector(dimension, normed = True):
    """
        Generates a sample from the D-dimensional N(0, 1/D)
        if normed == True, the result is being normed 
            (equivalent to uniform sampling on the unit (D-1)-dimensional sphere)
    """
    
    meanU = np.zeros(dimension)
    covU = 1 / dimension * np.eye(dimension)
    U = np.random.multivariate_normal(meanU, covU)[:, None]
    
    if normed:
        U /= np.linalg.norm(U)
        
    return U
    
def generate_random_classes_from_mixture_parameter(N, mixture_parameter):
    """
        Let p = len(mixture_parameter)
        Generates a N-array of {0,...,p-1} where each cell is filled with x=0,...,p-1 with probability given in mixture_parameter
        Ex : if N=10 and mixture_parameter=[0.4, 0.6]
        then a 10-cell array containing zeros and ones is returned, on average with 40% of zeros and 60% of ones
    """
    
    labels = np.random.random(N)[None, :]
    if abs(np.sum(mixture_parameter) - 1) > eps:
        raise Exception("Mixture parameters must sum to 1")
    cs = np.cumsum(mixture_parameter)[:, None]
    
    return np.argmax(labels < cs, axis=0)[None,:]
    
def generate_isotropic_noise_dataset(N, U, mixture_parameter, sigma, eta, ordered=False):
    """
        N is an integer
        U is a D-dimensional vector
        mixture_parameter is a 2-array = [a, b] summing to 1 (eg. [0.1, 0.9])
        sigma is a float
        eta is a float

        Generates a N-dataset of D-dimensional points (with corresponding labels) with 
            * (a)% of the N points centered around -U (label = -1)
            * (b)% of the N points centered around +U (label = +1)
        
        Gaussian samples around +U and -U, noise covariance : sigma*I_D
        
        A fraction (eta)% of the labels is masked and replaced with 0
        
        if ordered == True, all the labeled points are put first, leaving the unlabelled points at the end of the array
        
        Returns :
        ---------
        * V_learn : 1xN numpy array containing all labels (even the masked ones) : ground truth
        * S_learn : 1xN numpy array containing labels, with (1-eta)% of zeros (the masked ones)
        * Y_learn : DxN numpy array containing the N D-dimensional data samples
        * Z_learn : DxN numpy array containg the gaussian noises used to generate the data points
    """
    
    D = U.shape[0]
        
    V_learn = generate_random_classes_from_mixture_parameter(N, mixture_parameter) * 2 - 1 # For binary classification
    Z_learn = np.random.multivariate_normal(np.zeros(D), np.eye(D), size=N).T
    Y_learn = V_learn * U + sigma * Z_learn
    mask = np.random.binomial(1, eta, size = N)
    S_learn = V_learn.copy()
    S_learn[0, mask == 0] = 0
    
    if ordered:
        idxs_l = S_learn[0, :] != 0
        Vl = V_learn[0, idxs_l][None, :]
        Vu = V_learn[0, ~idxs_l][None, :]
        Sl = S_learn[0, idxs_l][None, :]
        Su = S_learn[0, ~idxs_l][None, :]
        Yl = Y_learn[:, idxs_l]
        Yu = Y_learn[:, ~idxs_l]
        Zl = Z_learn[:, idxs_l]
        Zu = Z_learn[:, ~idxs_l]
        V_learn = np.hstack((Vl, Vu))
        S_learn = np.hstack((Sl, Su))
        Y_learn = np.hstack((Yl, Yu))
        Z_learn = np.hstack((Zl, Zu))
    
    return V_learn, S_learn, Y_learn, Z_learn