# -*- coding: utf-8 -*-
"""
* File providing some tools to facilitate the study of
* random matrix distributions
"""

import numpy as np

def MP_pdf(c, sigma, x):
    """ Density of a Marchenko-Pastur distribution parametrized by c and sigma, evaluated at x """
    lbm = sigma ** 2 * (1 - np.sqrt(c))**2
    lbp = sigma ** 2 * (1 + np.sqrt(c))**2
    return np.sqrt((lbp - x) * (x - lbm)) / (2 * np.pi * c * sigma ** 2 * x)
    

def MP_pdf_vectorized(c, sigma):
    """ Numpy vectorized density of a Marchenko-pastur distribution parametrized by c and sigma """
    return np.vectorize(lambda x: MP_pdf(c, sigma, x))

def fit_MP(draws):
    """ 
        draws is a p-numpy array containing p samples
        This functions fits a good Marchenko-Pastur distribution which may have been used
        to generate these samples
        
        Returns c, sigma (good parameters for the MP distribution)
    """
    
    lpMP = np.max(draws)
    lmMP = np.min(draws)
    etendueMP = - lmMP + lpMP
    centreMP = (lmMP + lpMP) / 2
    
    Omega = centreMP / etendueMP
    Delta = 16**2*Omega**4 - 64*Omega**2
    cMP = ((16*Omega**2-2) - np.sqrt(Delta))/2
    sigmaMP = np.sqrt(centreMP / (1 + cMP))
    
    return (cMP, sigmaMP)