# -*- coding: utf-8 -*-
"""
* Script used to illustrate the concentration 
* of the norm of the MSE estimator for the fully supervised setting
"""

import numpy as np
import matplotlib.pyplot as plt
from dataset import generate_class_vector, generate_isotropic_noise_dataset
from tqdm import tqdm


D = 100
N = 100
p = 1000  # number of times we compute the norm of Umse
sigma = 1
alpha = N / D

# We compute norms twice : once for N,D and once for 2N,2D 
# to check that the variance decreases (ie. we have convergence)
norms = []
norms2 = []

for i in tqdm(range(p)):
    # Generate data and compute the norm
    # N, D
    U = generate_class_vector(D)
    V_learn, S_learn, Y_learn, Z_learn = generate_isotropic_noise_dataset(N, U, [0.5, 0.5], sigma, 1, ordered = True)
    Umse = 1 / (N + sigma**2 * D) * np.sum(V_learn * Y_learn, axis=1)
    norms.append(np.linalg.norm(Umse))
    
    # 2N, 2D
    U = generate_class_vector(2 * D)
    V_learn, S_learn, Y_learn, Z_learn = generate_isotropic_noise_dataset(2 * N, U, [0.5, 0.5], sigma, 1, ordered = True)
    Umse = 1 / (2 * N + sigma**2 * 2 * D) * np.sum(V_learn * Y_learn, axis=1)
    norms2.append(np.linalg.norm(Umse))

th_value = np.sqrt(alpha / (alpha + sigma**2))

plt.hist(norms, bins=int(np.sqrt(p)), label=f"N={N}")
plt.hist(norms2, bins=int(np.sqrt(p)), label=f"N={2*N}")
plt.axvline(x=th_value, c='red')
plt.title(f'{N} samples of the norm of Umse (theoretical value : {th_value:.2f})')
plt.legend()
