# -*- coding: utf-8 -*-
"""
* Script used to understand the origin of the threshold value
* which makes the performance of the approched SSL classifier
* abruptly decrease
"""

import numpy as np
import matplotlib.pyplot as plt
from dataset import generate_class_vector, generate_isotropic_noise_dataset

D = 800
N = 400
sigma = np.sqrt(0.9)
alpha = N / D
eta = 0.1

# Generating data
U = generate_class_vector(D)
_, S_learn, Y_learn, _ = generate_isotropic_noise_dataset(N, U, [0.5, 0.5], sigma, eta, ordered = True)
Yu = Y_learn[:, S_learn[0, :] == 0] # unlabelled data

# Plots
fig, ax = plt.subplots(2,3)
lbds = np.linspace(0.1, 0.7, 6)
for i in range(len(lbds)):
    lbd = lbds[i]
    S = lbd * Yu@Yu.T # covariance part
    ax[i//3, i%3].hist(np.linalg.eigh(S)[0], bins=int(np.sqrt(D)), density = True, label=str(lbd))
    ax[i//3, i%3].axvline(sigma**2*(N+sigma**2*D), c='red') # identity part
    ax[i//3, i%3].set_title(f"lambda = {lbd:.3f}")
    ax[i//3, i%3].set_yticks(())
plt.tight_layout()
