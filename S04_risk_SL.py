# -*- coding: utf-8 -*-
"""
* Script used to illustrate the performance of the Umse classifier in the fully
* supervised setting
"""

import numpy as np
import matplotlib.pyplot as plt
from benchmark import benchmark
from scipy.stats import norm
from information_toolbox import find_qstar_by_derivative
from classifiers import u_SL


D = 90
N = 100
sigma = 1
alpha = N / D
p=1000

# Compute performance for N,D
perfs1, names1 = benchmark(D, N, sigma, 1, {"SL" : u_SL} , nb_pass=p)
# And once again for 2N, 2D to check that the variance decreases (so that it converges)
perfs2, names2 = benchmark(2*D, 2*N, sigma, 1, {"SL" : u_SL} , nb_pass=p)
# Theoretical supervised risk according to Lelarge/Miolane (2019)
th_risk = 1-norm.cdf(np.sqrt(find_qstar_by_derivative(alpha, sigma, 1))/sigma)

plt.hist(perfs1[0], bins=30, label=f"N={N}", density=True)
plt.hist(perfs2[0], bins=30, label=f"N={2*N}", density=True)
plt.axvline(x=th_risk, c='red')
plt.title(f"{p} passes, theoretical risk {th_risk:.3f}")
plt.legend()
