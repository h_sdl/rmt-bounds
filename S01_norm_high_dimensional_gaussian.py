# -*- coding: utf-8 -*-
"""
* Script showing that the norm of a high-dimensional gaussian is essentially constant
"""

from scipy.stats import multivariate_normal
import numpy as np
import matplotlib.pyplot as plt

D = 100
N = 1000
samples = multivariate_normal(np.zeros(D), 1/D*np.eye(D)).rvs(N)
norms = np.linalg.norm(samples, axis=1)

plt.hist(norms, bins=30)
plt.title(f"Histogram of the norms of {N} {D}-dimensional gaussian samples")
