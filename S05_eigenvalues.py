# -*- coding: utf-8 -*-
"""
* Script aiming at illustrating the behaviour of the eigenvalues of the 
* resolvent of the sample covariance matrix
"""

import numpy as np
import matplotlib.pyplot as plt
from dataset import generate_class_vector, generate_isotropic_noise_dataset

D = 800
N = 400
sigma = np.sqrt(0.9)
alpha = N / D
eta = 0.1

# Generating data
U = generate_class_vector(D)
_, S_learn, Y_learn, _ = generate_isotropic_noise_dataset(N, U, [0.5, 0.5], sigma, eta, ordered = True)
Yu = Y_learn[:, S_learn[0, :] == 0]  # unlabelled data

Su = sigma**2 * (N + sigma**2 + D) * np.eye(D) - Yu@Yu.T

plt.hist(np.linalg.eigh(Su)[0], bins=int(np.sqrt(D)), density=True)
plt.title("Eigenvalues of the Su matrix")
plt.title("Valeurs propres de la matrice Su")
plt.yticks(())
plt.tight_layout()
