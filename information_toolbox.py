# -*- coding: utf-8 -*-
"""
* File used to estimate information-theoretic 
* quantities used in Lelarge and Miolane's paper
"""

import numpy as np
eps = 1e-6

def mmsev(x, n = 100000):
    """ 
        Estimates MMSE_v(x) according to Lelarge and Miolane's formula
        by peforming a Monte-Carlo simulation with n samples
    """
    z0 = np.random.normal(size = n)
    val = 1 - np.mean(np.tanh(np.sqrt(x) * z0 + x))
    return val
    
def find_qstar_by_derivative(alpha, sigma, eta):
    """
        Estimates q* for parameters alpha, sigma, eta
        according to Lelarge and Miolane's equation (page 7, just after equation (11))
        Performs a dichotomic search on the [0,1] interval to find the correct fixed point
    """
    
    # evaluation of the derivative f'[alpha, sigma, eta](q) thanks to the mmsev function
    def g(q):
        return alpha/(2 * sigma ** 2) * (1 - eta) * mmsev(q / sigma **2, n=100000) - alpha / (2 * sigma ** 2) + q / 2 / (1 - q)
    
    # Starts search on the [0, 0.99] interval
    a = 0
    b = 0.99
    
    for i in range(100): #  Stops after 100 iterations or after having reached a 1e-3 precision
        if abs(a - b) < 1e-3:
            break
        
        if g(b) < 0:
            a = b
            b = (1 + b) / 2
        else:
            c = (a + b) / 2
            if g(c) < 0:
                a = c
            else:
                b = c
    
    return (a + b) / 2

def find_qstar_mai_fixed_point(alpha, sigma, eta, n_draws = 500000, precision = 1e-5):
    """
        Estimates q* for parameters alpha, sigma, eta
        by using fixed point equation (59) of Mai/Couillet(2020) and a fixed point method.
        Iterates until the value converges (precision 1e-5 between two consecutive values)
    """
    norm_mu = 1 / sigma
    q = 1
    eps = 1
    while abs(eps) > precision:    
        q2 = norm_mu**2 - norm_mu**2 / (1+alpha*norm_mu**2*(1-(1-eta)*mmsev(q, n = n_draws)))
        eps = q2 - q
        q = q2
        
    return q*sigma**2
    
def find_qc_mai_fixed_point(alpha, sigma, eta, precision = 1e-4):
    """
        Estimates qc for parameters alpha, sigma, eta
        by using fixed point equation (61) of Mai/Couillet(2020) and a fixed point method.
        Iterates until the value converges (precision 1e-5 between two consecutive values)
    """
    norm_mu = 1 / sigma
    qc = 1
    eps = 1
    while abs(eps) > precision:    
        qc2 = norm_mu**2 - norm_mu**2 / (1 + alpha * norm_mu**2 * ( eta + (1 - eta)*(qc / (1 + qc))))
        eps = qc2 - qc
        qc = qc2
    
    return qc*sigma**2

