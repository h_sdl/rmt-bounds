# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 12:57:44 2020

@author: Hugues
"""

import numpy as np
from scipy import optimize

# Supervised classifier : takes Y,S and sums all the Sigma(vi*Yi)
def u_SL(Y,V,_):
    return np.sum(V * Y, axis=1)

# Approximate SSL classifier (using gaussian approximation for u|y,s)
def u_approx_SSL(Y, S, Gram, sigma, lbd):
    D, N = Y.shape

    M = np.sum(S * Y, axis=1)[:, None]
    Cov = 1 / (sigma**2 * (N + 1 + sigma**2 * D)) * ((S == 0).astype(int) * Y).dot(((S == 0).astype(int) * Y).T)
    
    Q = np.linalg.inv(np.eye(D) - lbd * Cov)
    return Q @ M

# Maximum a posteriori (MAP) classifier (using the cosh form of u|y,s)
def ucosh(Y, S, Gram, sigma):
    A = np.sum(S * Y, axis = 1)
    Yu = Y[:, S[0,:] == 0]
    D, N = Y.shape
        
    def f(u):
        r = 0
        for i in range(np.shape(Yu)[1]):
            r += np.log(np.cosh(1 / sigma**2 * u @ Yu[:, i]))
        return (N + sigma**2 * D) / (2 * sigma**2) * (u @ u - 2 / (N + sigma**2 * D) * u @ A) - r
    
    resmini = optimize.minimize(f, A / (N + sigma**2 * D))
    return resmini.x


# Helper functions
def normalized_USL(V, Y, sigma):
    D, N = np.shape(Y)
    return 1 / (N + sigma ** 2 * D) * np.sum(V * Y, axis=1)[:, None]
def energy_USL(U, N, sigma):
    return (N + sigma ** 2 * D) / (2 * sigma ** 2) * np.linalg.norm(U) ** 2


def stochastic_u(Y, S, Gram, sigma, p = 100):
    """ 
        Computes p high-norm estimators using the stochastic 
        "compute_maxU_stochastic" function and aggregates them
        using the exp(||u||^2) weights
    """
    N = Y.shape[1]
    ests = [] # estimators
    ws = [] # weights
    for i in range(p):
        Vbest = compute_maxU_stochastic(Y, S, Gram, sigma, perturbation_rate = 0.1, mute = True)
        Ubest = normalized_USL(Vbest, Y, sigma)
        ests.append(Ubest)
        ws.append(energy_USL(Ubest, N, sigma))
    w = np.stack(np.exp(ws)) / np.sum(np.exp(ws))
    return (np.hstack(ests) * w).sum(axis = 1)

def compute_maxU_stochastic(Y, S, Gram, sigma, perturbation_rate = 0.1, mute = False):
    """ 
        Computes a stochastic high-norm U
        by using a kind of discrete gradient ascent algorithm 
        (relying on the Gram matrix of the data samples)
        Very surely finds local maxima (the discrete character of 
        V makes this behaviour highly probable)
        Returns the best found V
    """
    idxs_zeros = np.where(S == 0)[1]
    idxs_nonzero = np.where(S != 0)[1]
    N = Y.shape[1]
    
    V = (np.random.binomial(1, 0.5, size = N) * 2 - 1)[None, :]
    
    while True:
        V[0, idxs_nonzero] = S[0, idxs_nonzero] # making sure V contains the true labels for the known ones
        if not mute: print(energy_USL(normalized_USL(V, Y, sigma), N, sigma))
        
        # Deciding which labels should be perturbated (stochastic)
        nb_perturb = int((S.shape[1] - len(idxs_nonzero)) * perturbation_rate)
        idxs_perturb = np.unique(np.random.choice(idxs_zeros, nb_perturb))
        
        # Makes a copy and perturbates it
        V2 = V.copy()
        V2[0, idxs_perturb] *= -1
        
        # Iterating on the original V and on its perturbation
        signs = np.sign(np.sum(V * Gram, axis=1))[None, :]
        signs2 = np.sign(np.sum(V2 * Gram, axis=1))[None, :]
        
        # Choosing the highest norm V between the original one and its copy
        if energy_USL(normalized_USL(signs, Y, sigma), N, sigma) < energy_USL(normalized_USL(signs2, Y, sigma), N, sigma):
            signs = signs2
        
        # When the algorithm stabilizes, exiting the loop
        if np.sum(np.sum((V != signs) & (S == 0))) == 0:
            break
        
        # Updating V
        V = signs
        
    return V

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from benchmark import benchmark
    from information_toolbox import find_qstar_by_derivative
    from scipy.stats import norm
    
    D = 90
    N = 100
    sigma = 1
    eta = 0.1
    alpha = N / D
    p=100
    
    fcts = {
            "SL" : u_SL, 
            "SSL": lambda Y, S, G: u_approx_SSL(Y, S, G, sigma, 0.5), 
            "sto": lambda Y, S, G: stochastic_u(Y, S, G, sigma),
            #"MAP": lambda Y, S, G: ucosh(Y, S, G, sigma),
           }
    perfs, names = benchmark(D, N, sigma, eta, fcts , nb_pass=p)

    # Theoretical supervised risk according to Lelarge/Miolane (2019)
    th_risk = 1-norm.cdf(np.sqrt(find_qstar_by_derivative(alpha, sigma, eta)) / sigma)
    th_risk_SL = 1-norm.cdf(np.sqrt(find_qstar_by_derivative(alpha * eta, sigma, 1)) / sigma)
    
    for i in range(len(perfs)):
        plt.plot(perfs[i,:], label=names[i])
        
    plt.plot(th_risk * np.ones(p), label="theoretical SSL")
    plt.plot(th_risk_SL * np.ones(p), label="theoretical SL")
    
    plt.legend()
        