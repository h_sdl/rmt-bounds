# -*- coding: utf-8 -*-
"""
* Script used to plot the risk of the approximate SSL classifier for several
* values of the regularization parameter lambda
"""

import numpy as np
import matplotlib.pyplot as plt
from benchmark import benchmark
from scipy.stats import norm
from information_toolbox import find_qstar_by_derivative
from classifiers import u_SL, u_approx_SSL


D = 800
N = 400
eta = 0.1
sigma = np.sqrt(0.9)
alpha = N / D

nb_pass = 2
fs = { "SL" : u_SL }
lbds = np.linspace(0, 1, 50)
for lbd in lbds:
    # the lambda function needs to be evaluated at lambda in order to prevent
    # a strange behaviour that makes all lambda functions have the same lambda value
    fs[lbd] = (lambda l: (lambda Y,S,G: u_approx_SSL(Y, S, G, sigma, l)))(lbd)

perfs, names = benchmark(D, N, sigma, eta, fs, nb_pass = nb_pass)
names = np.array(names)

# Generating series
perfs_SSL = perfs[(names != "SL") & (names != "oracle")].mean(axis=1)
perf_emp_SL = np.mean(perfs[names=="SL",:])
serie_perf_emp_SL = perf_emp_SL * np.ones_like(lbds)
perf_th_SL = 1 - norm.cdf(np.sqrt(find_qstar_by_derivative(alpha * eta, sigma, 1)) / sigma)
serie_perf_th_SL = perf_th_SL * np.ones_like(lbds)
perf_th_SSL = 1 - norm.cdf(np.sqrt(find_qstar_by_derivative(alpha, sigma, eta)) / sigma)
serie_perf_th_SSL = perf_th_SSL * np.ones_like(lbds)

# Plotting
plt.plot(lbds, serie_perf_th_SSL, label="SSL théorique")
plt.plot(lbds, serie_perf_th_SL, label="SL théorique")
plt.plot(lbds, serie_perf_emp_SL, label="SL empirique")
plt.plot(lbds, perfs_SSL, label="SSL empirique")

plt.xlabel("lambda")
plt.ylabel("Risque")

plt.legend()
plt.tight_layout()