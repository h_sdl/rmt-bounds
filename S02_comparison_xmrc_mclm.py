# -*- coding: utf-8 -*-
"""
* Script used to compare Mai and Couillet's performance to the bayes optimal performance derived by Lelarge and Miolane
"""

import numpy as np
from scipy.stats import norm
from tqdm import tqdm
import matplotlib.pyplot as plt
from information_toolbox import find_qstar_mai_fixed_point, find_qc_mai_fixed_point

etas = np.linspace(0, 1, 200)
alpha = 1
sigma = np.sqrt(0.9)
res = np.zeros((len(etas), 2))
for i in tqdm(range(len(etas))):
    q = find_qstar_mai_fixed_point(alpha, sigma, etas[i], n_draws = 10000000, precision=5e-5)
    qc = find_qc_mai_fixed_point(alpha, sigma, etas[i])
    res[i,:] = 1 - norm.cdf(np.sqrt(q) / sigma), 1 - norm.cdf(np.sqrt(qc) / sigma)
    
fig, ax1 = plt.subplots()
ax1.set_xlabel('eta (proportion de données étiquetées)') # eta (proportion of unlabelled data)
ax1.set_ylabel('Risque (espérance de la proportion d\'erreurs)') # Risk (expected proportion of errors)
ax1.plot(etas, res[:, 0], label="Bayésien (Lelarge et Miolane)") # Bayesian (Lelarge and Miolane)
ax1.plot(etas, res[:, 1], label="Similarités centrées (Mai et Couillet)") # Centered similarities (Mai and Couillet)
ax1.tick_params(axis='y')
plt.legend()

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:green'
ax2.set_ylabel('Différence relative (%)', color=color) # Relative difference (%)
ax2.plot(etas, abs(res[:, 0] - res[:, 1]) / res[:, 1] * 100, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()
