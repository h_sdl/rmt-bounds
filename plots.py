# -*- coding: utf-8 -*-
"""
Created on Tue May 12 10:05:48 2020

@author: Hugues Souchard de Lavoreille
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

d1 = 0
d2 = 1

count_contours = 0
cmaps = [cm.Greys, cm.Greys, cm.Purples, cm.Blues, cm.Greens, cm.Oranges, cm.Reds]

def define_dims(a, b):
    global d1, d2
    d1 = a
    d2 = b
    
def scatter(points, colors):
    global d1, d2
    plt.scatter(points[d1, :], points[d2, :], c=colors[0, :])
    plt.axis('equal')
    
def plot_vector(v):
    global d1, d2
    plt.arrow(0, 0, float(v[d1]), float(v[d2]))
    
def contour_function(f, vmin = -5, vmax = 5, resolution = 1000, no_vectorization = False):
    global count_contours, cmaps
    xx, yy = np.meshgrid(np.linspace(vmin, vmax, resolution), np.linspace(vmin, vmax, resolution))
    pts = np.hstack((xx.reshape((-1,1)), yy.reshape((-1,1))))
    
    if no_vectorization:
        vals = np.apply_along_axis(f, 1, pts).reshape((resolution, resolution))
    else:
        vals = f(pts).reshape((resolution, resolution))
    
    alpha = 0.5
    if count_contours == 0: alpha = 1
    
    plt.contourf(xx, yy, vals, alpha=alpha, cmap=cmaps[count_contours % len(cmaps)])
    plt.axis('equal')
    
    count_contours = count_contours + 2
