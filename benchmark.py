# -*- coding: utf-8 -*-
"""
* Utility file for benchmarking classifiers
"""

import numpy as np
from dataset import generate_isotropic_noise_dataset, generate_class_vector
from tqdm import tqdm

def benchmark(D, N, sigma, eta, functions, nb_pass = 100, N_test = 100000):
    """
        Evaluates the risk for a collection of 
        classifiers (functions) by computing it nb_pass times and averaging it
        
        Each time, the risk will be evaluated by using N_test samples
        
        functions is a dict of {'classifier name': function taking (Y, S, Gram) }
            * Y is the dataset
            * S are the label (including the potentially masked ones)
            * Gram is a precomputed Gram matrix to decrease computation time if the Gram matrix is needed
              (scalar product of data points, NxN matrix)
              
        Returns the computed risks in a len(functions) x nb_pass numpy array, as well as the names of the classifiers in the same order
    """
    
    perfs = np.zeros((len(functions) + 1, nb_pass))
    functions_names = list(functions.keys()) + ["oracle"]
    
    U = generate_class_vector(D)
    
    V_test, _, Y_test, _ = generate_isotropic_noise_dataset(N_test, U, [0.5, 0.5], sigma, eta)
    
    for i in tqdm(range(nb_pass)):
        V_learn, S_learn, Y_learn, Z_learn = generate_isotropic_noise_dataset(N, U, [0.5, 0.5], sigma, eta, ordered = True)
        Gram = Y_learn.T.dot(Y_learn)
        j = 0
        for name_function, function in functions.items():
            uest = function(Y_learn, S_learn, Gram)
                
            perfs[j, i] = np.mean(np.sign(Y_test.T.dot(uest)).T != V_test)
            j += 1
            
        perfs[j, i] = np.mean(np.sign(Y_test.T.dot(U)).T != V_test)
    
    return perfs, functions_names